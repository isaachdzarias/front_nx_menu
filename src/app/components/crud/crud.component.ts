import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatTableDataSource } from '@angular/material';
import { AlertService, ConfirmationService } from '@next/nx-controls-common';
import { UserCrud } from 'src/app/models/UserCrud';
import { CrudUserService } from 'src/app/service/CrudUser.service';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.scss']
})
export class CrudComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;
  displayedColumns: string[] = ['brm', 'nombre', 'puesto', 'img','actions'];
  dataSource : MatTableDataSource<UserCrud>;

  base64:string = "";
  loading:boolean = false;

  user: UserCrud = new UserCrud();
  userEdit: UserCrud = new UserCrud();
  showEditView: boolean = false;

  constructor(
    private userService:CrudUserService,
    private alertService: AlertService,
    private confirmationService: ConfirmationService
  ){}

  ngOnInit() {
    this.getUsers();
  }

  setEditUser(u:UserCrud){
    this.showEditView = true;
    this.userEdit = u;
  }

  getUsers(){
    this.userService.getAllUsers().subscribe(
      ul=>{
        ul.forEach(element => {
          element.img = element.img.substr(0,15) + "...";
          element.base64 = 'data:image/jpeg;base64,'+element.base64;
        });
        this.dataSource = new MatTableDataSource<UserCrud>(ul);
        this.dataSource.paginator = this.paginator;
        this.user = new UserCrud();
        this.base64 = "";
        this.loading = false;
      }
    )
  }

  addUser(){
    this.loading = true;
    if(this.user !== null){
     if(this.user.brm){
      if(this.user.puesto){
        if(this.base64){
          if(this.user.nombre){
            this.userService.createUser(this.user).subscribe(e=>{
              this.showSuccess("Usuario agregado exitosamente");
              this.getUsers();
            })
          }else{
            this.showFieldEmpty("Falta nombre");
          }
        }else{
          this.showFieldEmpty("Falta foto de perfil");
        }
      }else{
        this.showFieldEmpty("Falta puesto");
      }
     }else{
        this.showFieldEmpty("Falta BRM");
     }
    }else{
      this.showFieldEmpty("Rellene todos los campos");
    }
  }

  showFieldEmpty(message:string){
    this.loading = false;
    this.showWarn(message);
  }

  setImg(event:any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.base64 = reader.result.toString();
      this.user.img = event.target.files[0].name;
      var baseToUpload : string = reader.result.toString();
      let indexFinishType = baseToUpload.indexOf(',');
      baseToUpload = baseToUpload.substr(indexFinishType+1,baseToUpload.length-1);
      this.user.base64 = baseToUpload;
    };
  }

  deleteUser(id:number):void{
    this.confirmationService.confirm({
      message: '¿Está seguro de eliminar este usuario?',
      lblOkBtn: 'Si',
      lblCancelBtn: 'Cancelar',
      accept: () => {
        this.userService.deleteUser(id).subscribe(res=>{
          this.showSuccess("Usuario eliminado correctamente");
          this.getUsers();
        })
      },
      reject: () => {
      }
    });
  }

  setName(event:any){
    if(this.showEditView){
      this.userEdit.nombre = event.target.value;
    }else{
      this.user.nombre = event.target.value;
    }
  }

  setBrm(event:any){
    if(this.showEditView){
      this.userEdit.brm = event.target.value;
    }else{
      this.user.brm = event.target.value;
    }
  }

  setPuesto(event:any){
    if(this.showEditView){
      this.userEdit.puesto = event.target.value;
    }else{
      this.user.puesto = event.target.value;
    }
  }

  //Edit
  setImgEdit(event:any) {
    const file = event.target.files[0];
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      this.userEdit.base64 = reader.result.toString();
      this.userEdit.img = event.target.files[0].name;
    };
  }

  showWarn(message:string):void {
    this.alertService.warn(message);
  }

  showSuccess(title:string): void {
    this.alertService.success(title);
  }

  showEdit(status:boolean):void{
    this.showEditView = status;
  }

  updateUser(u:UserCrud):void{
    if(u.brm){
      if(u.puesto !== "" && u.puesto !== "Selecciona un puesto"){
        if(u.base64 !== ""){
          if(u.nombre !== ""){
            let indexFinishType = u.base64.indexOf(',');
            u.base64 = u.base64.substr(indexFinishType+1,u.base64.length-1);
            this.userService.updateUser(u).subscribe(e=>{
              this.showSuccess("Usuario actualizado exitosamente");
              this.getUsers();
              this.showEditView = false;
            })
          }else{
            this.showWarn("Falta nombre");
          }
        }else{
          this.showWarn("Falta foto de perfil");
        }
      }else{
        this.showWarn("Falta puesto");
      }
    }else{
      this.showWarn("Falta BRM");
    }
  }
}
