import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { ServiceLoginService } from 'src/app/service/service-login.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {

  user:User = new User();

  constructor(private userService:ServiceLoginService,private router:Router) { }

  ngOnInit() {
    let userTemp : any = localStorage.getItem('usuario');
    if(userTemp){
      if(userTemp !== "null"){
        this.router.navigate(['/']);
      }
    }
  }

  login():void{
    if(this.user.username){
      if(this.user.password){
        this.userService.auth(this.user).subscribe(e=>{
          console.log("Entra?");
          console.log(e);
          localStorage.setItem('usuario',e.username);
          localStorage.setItem('rol',e.rol);
          this.router.navigate(['/']);
        });
      }else{
        alert("La contraseña es requerida");
      }
    }else{
      alert("El nombre de usuario es requerido");
    }
  }

}
