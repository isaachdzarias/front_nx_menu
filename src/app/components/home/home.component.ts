import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  leftContent: any = [
    { description: 'Menú', isTitle: true },

  ];
  user:User = new User();
  constructor(public router: Router) {}

  logOut():void{
    localStorage.setItem('usuario',null);
    localStorage.setItem('rol',null);
    this.validateSession();
  }

  validateSession():void{
    let userTemp : any = localStorage.getItem('usuario');
    if(userTemp){
      if(userTemp !== "null"){
        this.user.username = userTemp;
        this.user.rol = localStorage.getItem("rol");
        if (this.user.rol == "jugador") {
          this.leftContent.push({ description: 'Ir a Juego', isTitle: false, route:'jugador'});
        } else {
          this.leftContent.push({ description: 'Ir a CRUD empleados', isTitle: false, route:'empleado'});
        }
      }else{
        this.router.navigate(['/login']);
      }
    }
  }

  ngOnInit() {
    this.validateSession();
  }

}
