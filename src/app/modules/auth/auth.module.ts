import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AuthenticationComponent } from 'src/app/components/authentication/authentication.component';
import { AuthRoutingModule } from './auth-routing.module';

import { FrameworkModule } from '@next/nx-core';
import { FlexLayoutModule } from '@angular/flex-layout';

const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '/api',
  application: 'BANREGIOWEBAPP',
  applicationTitle: 'Banregio Web App'
};


@NgModule({
  declarations: [
    AuthenticationComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    FormsModule,
    HttpClientModule,
    FrameworkModule.forRoot(config),
    FlexLayoutModule
  ]
})
export class AuthModule { }
