import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FrameworkModule } from '@next/nx-core';
import { CommonsModule } from '@next/nx-controls-common';
import { JuegoRoutingModule } from './juego-routing.module';
import { JuegoComponent } from 'src/app/components/juego/juego.component';

const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '/api',
  application: 'BANREGIOWEBAPP',
  applicationTitle: 'Banregio Web App'
};

@NgModule({
  declarations: [
    JuegoComponent
  ],
  imports: [
    CommonModule,
    JuegoRoutingModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot()
  ]
})

export class JuegoModule { }
