import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FrameworkModule } from '@next/nx-core';
import { CommonsModule } from '@next/nx-controls-common';
import { FormsModule } from '@angular/forms';
import {FlexLayoutModule} from "@angular/flex-layout";

import { EmpleadoComponent } from 'src/app/components/empleado/empleado.component';
import { EmpleadoRoutingModule } from './empleado-routing.module';
import { CrudComponent } from 'src/app/components/crud/crud.component';

//Components material
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { MatInputModule } from '@angular/material/input';

const config = {
  usernameLabel: 'BRM',
  usernamePlaceholder: 'Usuario',
  endpoint: '/api',
  application: 'BANREGIOWEBAPP',
  applicationTitle: 'Banregio Web App'
};

@NgModule({
  declarations: [
    EmpleadoComponent,
    CrudComponent
  ],
  imports: [
    CommonModule,
    EmpleadoRoutingModule,
    FrameworkModule.forRoot(config),
    CommonsModule.forRoot(),
    MatCardModule,
    MatTableModule,
    MatPaginatorModule,
    MatInputModule,
    MatDialogModule, 
    MatButtonModule,
    FormsModule,
    FlexLayoutModule
  ]
})
export class EmpleadoModule { }
