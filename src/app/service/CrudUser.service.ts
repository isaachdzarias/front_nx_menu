import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserCrud } from '../models/UserCrud';

@Injectable({
  providedIn: 'root'
})
export class CrudUserService {

  private url : string = "https://isaac-spring-admin.herokuapp.com/usuario/";
  constructor( private http:HttpClient) { }

  //Obtener usuarios
  getAllUsers():Observable<UserCrud[]>{
    return this.http.get<UserCrud[]>(this.url);
  }

  //Crear usuario
  createUser(u:UserCrud):Observable<UserCrud>{
    return this.http.post<UserCrud>(this.url,u);
  }

  //Obtener un usuario
  getUserById(id:number):Observable<UserCrud>{
    return this.http.get<UserCrud>(this.url+id);
  }

  //Actualizar
  updateUser(u:UserCrud):Observable<UserCrud>{
    return this.http.put<UserCrud>(this.url,u);
  }

  //Eliminar
  deleteUser(id:number):Observable<UserCrud>{
    return this.http.delete<UserCrud>(this.url+id);
  }

}